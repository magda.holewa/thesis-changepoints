from datetime import timedelta
from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd
import sys, pickle
from methods import get_changepoints, load_csv_timeseries, group_data


def local_regression(data, point, offset=3):
    start = point - timedelta(days=offset)
    end = point + timedelta(days=offset)

    nhood = data[start:end]
    nhood_y = [n.timestamp() / 86400 for n in list(nhood.index)]

    reg = LinearRegression().fit(y=nhood.values.reshape(-1,1),
                                X=np.array(nhood_y).reshape(-1,1))
    return reg.coef_[0][0], start, end


def summarize_data(data, classifier=None, kernels=None, **kwargs):
    data = data.drop(columns=["count"])
    for column in data:
        try:
            cpts = get_changepoints(data, [column], data.index[0], data.index[-1],
                                    classifier=classifier, kernels=kernels, **kwargs)
        
            for c in cpts:
                coef, start, end = local_regression(data[column], c)
                if coef < 0:
                    type = "spadek"
                else:
                    type = "wzrost"
                if (abs(coef) < 1.0):
                    print(f"Niewielki {type} od {start} do {end} dla tematu {column}")
                elif (abs(coef) > 10.0):
                    print(f"Duży {type} od {start} do {end} dla tematu {column}")
                else:
                    print(f"{type} od {start} do {end} dla tematu {column}")
        except:
            pass


if __name__ == '__main__':
    # args: path to data, path to classifier, path to kernels

    data = load_csv_timeseries(sys.argv[1])

    if len(sys.argv) > 3:
        with open(sys.argv[2], 'rb') as fp:
            classifier = pickle.load(fp)
        with open(sys.argv[3], 'rb') as fp:
            kernels = pickle.load(fp)
    else:
        classifier = None
        kernels = None

    # TODO: change to best algorithm
    summarize_data(data,
        classifier, kernels, draw=False, beta=1.0, width=40, min_size=500, algo='binseg')