import ruptures as rpt
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import IsolationForest
from rocket_functions import generate_kernels, apply_kernels


def load_csv_timeseries(name):
  df = pd.read_csv(name, index_col=0)
  df.index = pd.to_datetime(df.index)
  return df


def group_data(data, by='day'):
    if by == 'day':
        gb = data.groupby(lambda x : x.replace(minute=0, hour=0))
    elif by == 'hour':
        gb = data.groupby(lambda x : x.replace(minute=0))
    else:
        raise ValueError(by)
    return gb.sum()#.drop(columns=['time'])


def data_ratio(data):
    subdata = data.drop(columns=['count'])
    arr = subdata.to_numpy()
    counts = list(data['count'])
    arr = arr.astype('float')
    for i, row in enumerate(arr):
        arr[i] = row / counts[i] if counts[i] > 0 else row
    return pd.DataFrame(arr, index=subdata.index, columns=subdata.columns)


def transform_data(data, cpts, chunk_size=7):
    """ Transforms data and changepoint dict into x/y data for ROCKET """
    ts_data = []
    y_data = []

    examples = list(cpts.keys())

    for ex in examples:
        points = [pd.to_datetime(p) for p in cpts[ex]]
        for start in range(0, len(data), chunk_size):
            df_subset = data[ex].iloc[start:start + chunk_size]
            if len(df_subset) != chunk_size:
                continue
            ts_data.append(list(df_subset))

            is_change = 1
            for p in points:
                if p >= list(df_subset.index)[0] and p <= list(df_subset.index)[-1]:
                    is_change = -1
                    break

            y_data.append(is_change)

    x_data = np.array(ts_data)
    y_data = np.array(y_data)

    # normalize
    x_data = (x_data - x_data.mean(axis = 1, keepdims = True)) #/ x_data.std(axis = 1, keepdims = True)
    stds = x_data.std(axis = 1, keepdims = True)

    with np.errstate(divide='ignore', invalid='ignore'):
        x_data = np.true_divide(x_data,stds)
        x_data[x_data == np.inf] = 0
        x_data = np.nan_to_num(x_data)

    return x_data, y_data


def forest_train(x_train, num_kernels=100):
    kernels = generate_kernels(x_train.shape[1], 100)
    print(len(kernels))
    x_train_transform = apply_kernels(x_train, kernels)

    classifier = IsolationForest(contamination=0.1, n_estimators=300)
    classifier.fit(x_train_transform)

    return classifier, kernels


def forest_predict(x_test, classifier, kernels):
    x_test = apply_kernels(x_test, kernels)
    return classifier.predict(x_test)


def forest_predict_dates(data, topic, classifier, kernels, chunk_size=7):
    points = []

    for start in range(0, len(data), chunk_size):
        df_subset = data[topic].iloc[start:start + chunk_size]
        if len(df_subset) != chunk_size:
            continue

        x_data = np.array([list(df_subset)])
        x_data = (x_data - x_data.mean(axis = 1, keepdims = True)) #/ x_data.std(axis = 1, keepdims = True)
        stds = x_data.std(axis = 1, keepdims = True)

        with np.errstate(divide='ignore', invalid='ignore'):
            x_data = np.true_divide(x_data,stds)
            x_data[x_data == np.inf] = 0
            x_data = np.nan_to_num(x_data)
            
        x_data = apply_kernels(x_data, kernels)

        result = classifier.predict(x_data)[0]
        if result == -1:
            points.append(chunk_size // 2 + start)

    points.append(len(data))
    return points


def get_changepoints(data, topics, start, end, algo="window",
                     cost="l2", beta=0.5, draw=True, 
                     chunk_size=7, classifier=None, kernels=None,
                     plot_height=6, return_index=False, **kwargs):
    subdata = data.loc[start:end, topics]
    arr = subdata.to_numpy()
    stds = [subdata[t].std() for t in topics]

    if algo == 'forest':
        # TODO: implement multidimensional detection
        assert len(topics) == 1

        # with open(model_path, 'rb') as model_f:
        #   classifier = pickle.load(model_f)

        # with open(kernel_path, 'rb') as kernel_f:
        #   kernels = pickle.load(kernel_f)

        bkps = forest_predict_dates(subdata, topics[0], classifier, kernels, chunk_size=chunk_size)

    else:
        if algo == 'window':
            algo = rpt.Window(model=cost, width=kwargs.get('width', 10)).fit(arr)
        elif algo == 'pelt':
            algo = rpt.Pelt(model=cost, min_size=kwargs.get('min_size', 5)).fit(arr)
        elif algo == 'binseg':
            algo = rpt.Binseg(model=cost, min_size=kwargs.get('min_size', 5)).fit(arr)
        elif algo == 'bottomup':
            algo = rpt.BottomUp(model=cost, min_size=kwargs.get('min_size', 5)).fit(arr)

        bkps = algo.predict(
            pen=kwargs.get('penalty', 
                        beta * np.log(len(subdata)) * pd.Series(stds).mean() ** 2))

    if draw:
        fig, axes = rpt.show.display(arr, bkps, bkps, figsize=(20, plot_height))
        plt.tick_params(axis='x', rotation=45)
        plt.xticks(bkps, [subdata.index[int(l)].date() for l in bkps[:-1]
                        if int(l) < len(subdata)])
        for i, a in enumerate(axes):
            a.title.set_text(topics[i])
            plt.margins(0.01)
            plt.show()

    if not return_index:
        dates = [subdata.index[int(l)].to_pydatetime() for l in bkps[:-1]
                        if int(l) < len(subdata)]
    else:
        dates = bkps
    return dates
