import csv
import pandas as pd
import datetime
import requests
import io, re, os, sys
from tqdm import tqdm
import zipfile


csv.field_size_limit(10000000)


def datetime_to_gdelt_code(dtime):
    return ((((dtime.year * 100 + dtime.month) * 100 + dtime.day) * 100 + dtime.hour) * 100 + dtime.minute) * 100 


def filter_and_save_csv(input_fp, out_path):
    with open(out_path, 'w', encoding="ISO-8859-1") as out_fp:
        writer = csv.writer(out_fp)
        reader = csv.reader(io.TextIOWrapper(input_fp, encoding="ISO-8859-1"), delimiter = '\t',
                           quoting=csv.QUOTE_NONE)
        for row in reader:
            if len(row) == 27 and (".pl" in row[3] or "srclc:pol" in row[25]):
                writer.writerow(row)


def download_files_from_time_range(date1, date2, results_dir, overwrite=False):
    if not os.path.isdir(results_dir):
        os.mkdir(results_dir)

    url = 'http://data.gdeltproject.org/gdeltv2/masterfilelist-translation.txt'
    myfile = requests.get(url)
    fileslist = [x.split(' ')[2] for x in myfile.text.split('\n') if len(x) > 40]
    fileslist = [file for file in fileslist if 'gkg' in file]

    pattern = re.compile('[0-9]{14}')
    files_to_download = [x for x in fileslist if int(pattern.findall(x)[0]) <= datetime_to_gdelt_code(date2)
                        and int(pattern.findall(x)[0]) >= datetime_to_gdelt_code(date1)]

    if not overwrite:
        dates_in_dir = [pattern.findall(x)[0] for x in os.listdir(results_dir)]
        files_to_download = [file for file in files_to_download if not pattern.findall(file)[0] in dates_in_dir]

    for filename in tqdm(files_to_download):
        try:
            r = requests.get(filename)
        except:
            print("Error during download: {}, retrying...".format(filename))
            r = requests.get(filename)

        try:
            with zipfile.ZipFile(io.BytesIO(r.content)) as zf:
                if len(zf.namelist()) > 0:
                    csv_name = zf.namelist()[0]
                    path_to_csv = os.path.join(results_dir, csv_name)
                    with zf.open(csv_name) as csv_file:
                        filter_and_save_csv(csv_file, path_to_csv)
        except:
            print("Error after download: {}".format(filename))


if __name__ == '__main__':
    date1 = datetime.datetime.fromisoformat(sys.argv[1])
    date2 = datetime.datetime.fromisoformat(sys.argv[2])
    print(date1)
    print(date2)
    results_dir = sys.argv[3]
    download_files_from_time_range(date1, date2, results_dir)