import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt
from tqdm import tqdm
import os, re, pickle
import sys
import csv
import requests
import datetime

csv.field_size_limit(10000000)

def extract_themes():
    """ Downloads and extracts the list of themes defined in GDELT """
    url = 'http://data.gdeltproject.org/documentation/GCAM-MASTER-CODEBOOK.TXT'
    r = requests.get(url)
    themes = [s.split('\t')[6] for s in r.text.split('\n') if len(s) > 0 and 'Themes' in s.split('\t')[5]]
    return themes


def datetime_to_gdelt_code(dtime):
    return ((((dtime.year * 100 + dtime.month) * 100 + dtime.day) * 100 + dtime.hour) * 100 + dtime.minute) * 100 


def gdelt_code_to_datetime(gcode):
    gcode = str(gcode)
    return datetime.datetime(int(gcode[:4]), int(gcode[4:6]), int(gcode[6:8]), int(gcode[8:10]), int(gcode[10:12]))


def summary_by_package(data_dir, result_path):
    frame_top = []
    all_files = os.listdir(data_dir)
    all_files.sort()
    themes = extract_themes()

    pattern = re.compile('[0-9]{14}')
    date_start = gdelt_code_to_datetime(pattern.findall(all_files[0])[0])
    date_end = gdelt_code_to_datetime(pattern.findall(all_files[-1])[0])

    time_points = []
    date = date_start
    while date <= date_end:
        time_points.append(date)
        date = date + datetime.timedelta(minutes=15)

    for timestamp in tqdm(time_points):
        time_code = datetime_to_gdelt_code(timestamp)
        filename = f"{time_code}.translation.gkg.csv"
        batch_data = dict()
        # batch_data["time"] = time_code
        for theme in themes:
            batch_data[theme] = 0

        try:
            with open(os.path.join(data_dir, filename), 'r', encoding="ISO-8859-1") as batch_file:
                reader = csv.reader(batch_file)
                
                batch_themes = []
                entries = 0
                for row in reader:
                    if len(row) > 7:
                        batch_themes += row[7].split(';')
                        entries += 1
            
            batch_data["count"] = entries
            batch_themes = [x for x in batch_themes if len(x) > 0 and not x == 'nan']
            counter_themes = Counter(batch_themes).most_common()
            for value, count in counter_themes:
                if value in batch_data.keys():
                    batch_data[value] = count
        except FileNotFoundError:
            print(f"File not found: {filename}")
            batch_data["count"] = 0

        # batch_data["time"] = time_code
        frame_top.append(batch_data)

    df = pd.DataFrame(frame_top, index=time_points)
    # with open(result_path + '.pkl', 'wb') as pf:
    #     pickle.dump(df, pf)
    df.to_csv(result_path + '.csv')
    return df


if __name__ == '__main__':
    # argv[1] - directory with GKG files, argv[2] - target file path (without extension)
    summary_by_package(sys.argv[1], sys.argv[2])