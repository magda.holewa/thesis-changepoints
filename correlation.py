from scipy.stats import spearmanr, pearsonr
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
from methods import load_csv_timeseries, data_ratio, group_data
import pandas as pd
import sys


def get_correlation_plot(df):
    themes = df.columns[:367]  # sample_data.columns[1:]
    df_divided = data_ratio(df)  # data_ratio(sample_data)
    data = df  # sample_data

    corr_list = []

    for t in tqdm(themes):
        corrs, _ = spearmanr(data[t], data['count'])
        corrsd, _ = spearmanr(df_divided[t], data['count'])
        
        corrp, _ = pearsonr(data[t], data['count'])
        corrpd, _ = pearsonr(df_divided[t], data['count'])
        
        c = dict()
        c["theme"] = t
        c["pearson"] = corrp
        c["pearson_div"] = corrpd
        c["spearman"] = corrs
        c["spearman_div"] = corrsd
        c["articles"] = data[t].sum()
        corr_list.append(c)

    corr_df = pd.DataFrame(corr_list)
    corr_df = corr_df.set_index("theme")

    # Pearson
    plt.plot(corr_df['pearson'], corr_df['articles'], 'g+', linewidth=0)
    plt.xlabel("Pearson metric")
    plt.xticks(np.arange(0, 1.01, step=0.1))
    plt.ylabel("Articles count")
    plt.show()

    # Spearman
    plt.plot(corr_df['spearman'], corr_df['articles'], 'g+', linewidth=0)
    plt.xlabel("Spearman metric")
    plt.xticks(np.arange(0, 1.01, step=0.1))
    plt.ylabel("Articles count")
    plt.show()

    return corr_df


if __name__ == '__main__':
    df = load_csv_timeseries(sys.argv[1])
    get_correlation_plot(df)
    get_correlation_plot(group_data(df))