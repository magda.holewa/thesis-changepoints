from methods import transform_data, forest_train, forest_predict
from sklearn.metrics import confusion_matrix

import sys
import pickle


def do_training(data, cpts, chunk_size=7, classifier_path='clas.pkl', kernel_path='kernels.pkl'):
    x_data, y_data = transform_data(data, cpts, chunk_size)
    classifier, kernels = forest_train(x_data)

    y_pred = forest_predict(x_data, classifier, kernels)
    print(confusion_matrix(y_data, y_pred))

    with open(classifier_path, 'wb') as fp:
        pickle.dump(classifier, fp)
    
    with open(kernel_path, 'wb') as fp:
        pickle.dump(kernels, fp)

    return classifier, kernels


if __name__ == '__main__':
    data = load_csv_timeseries(sys.argv[1])

    with open(sys.argv[2], 'rb') as fp:
        cpts = pickle.load(fp)

    do_training(data, cpts)