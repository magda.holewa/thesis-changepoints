import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys, pickle
from sklearn.model_selection import ParameterGrid
from methods import get_changepoints, load_csv_timeseries
from itertools import product
from ruptures.metrics import hausdorff, precision_recall, randindex
from time import time
from tqdm import tqdm
import pandas as pd


def run_hyperparam_grid(pg, data, true_pts, start, end, kernels=None, classifier=None, margin=5, filename="results.csv"):
    nums = {d : i for i, d in enumerate(data.index)}
    true_bkps = {th : [nums[i] for i in pd.to_datetime(pts)] + [len(data)] for th, pts in true_pts.items()}

    examples = list(true_bkps.keys())
    runs = []

    for run in tqdm(pg):
        for ex in examples:
            run_dict = dict()
            for arg, val in run.items():
                    run_dict[arg] = val
            start_time = time()
            #print(run)
            try:
                run_dict["data"] = ex
                points = get_changepoints(data, [ex], start, end, kernels=kernels, classifier=classifier, draw=False, return_index=True, **run)
                total_time = time() - start_time
                run_dict["time"] = total_time
                run_dict["points"] = points

                # Evaluation
                run_dict["hausdorff"] = hausdorff(true_bkps[ex], points) if len(points) > 1 else len(data)
                run_dict["rand"] = randindex(true_bkps[ex], points) if len(points) > 1 else 0
                prec, recall = precision_recall(true_bkps[ex], points, margin=margin)
                run_dict["precision"] = prec
                run_dict["recall"] = recall
                run_dict["f1"] = (2.0 * prec * recall) / (prec + recall) if prec != 0 and recall != 0 else 0
                
            except ValueError:
                pass
            runs.append(run_dict)

    rundf_q = pd.DataFrame(runs)
    rundf_q.to_csv(filename, index=False)

    return rundf_q


# params = [{
#     'algo': ['pelt','binseg','bottomup'],
#     'beta': [0.1, 0.2, 0.4, 0.5, 0.8, 1.0, 1.5],
#     'cost': ['l1', 'l2', 'rbf'],
#     'min_size': [2, 3, 5]
# },
# {
#     'algo': ['window'],
#     'beta': [0.1, 0.2, 0.4, 0.5, 0.8, 1.0, 1.5],
#     'cost': ['l1', 'l2', 'rbf'],
#     'width': [10, 15, 20, 25, 30, 35, 40, 45, 50]
# }]

params = [
{
    'algo': ['forest']
}]

# params = [
# {
#     'algo': ['window'],
#     'beta': [0.2, 0.4, 0.5, 0.8, 1.0, 1.5],
#     'cost': ['l1', 'l2', 'rbf'],
#     'width': [500, 1000]
# },
# {
#     'algo': ['binseg','bottomup'],
#     'beta': [0.4, 0.5, 0.8, 1.0, 1.5],
#     'cost': ['l1', 'l2'],
#     'min_size': [500, 1000]
# }
# # {
# #     'algo': ['pelt'],
# #     'beta': [0.8, 1.0],
# #     'cost': ['l1', 'l2'],
# #     'ratio': [True, False],
# #     'min_size': [500]
# # },
# ]

params_valid_full = [{
    'algo': ['pelt','binseg','bottomup'],
    'beta': [0.1, 0.2, 0.4, 0.5, 0.8, 1.0, 1.5],
    'cost': ['l1', 'l2'],
    'min_size': [200, 300, 500]
},
{
    'algo': ['window'],
    'beta': [0.1, 0.2, 0.4, 0.5, 0.8, 1.0, 1.5],
    'cost': ['l1', 'l2'],
    'width': [500, 1000, 1500]
},
{
    'algo': ['forest']
}]

if __name__ == '__main__':
    # args: path to data, path to changepoints, path to classifier, path to kernels
    pg = ParameterGrid(params_valid_full)

    data = load_csv_timeseries(sys.argv[1])

    with open(sys.argv[2], 'rb') as fp:
        cpts = pickle.load(fp)

    if len(sys.argv) > 4:
        with open(sys.argv[3], 'rb') as fp:
            classifier = pickle.load(fp)
        with open(sys.argv[4], 'rb') as fp:
            kernels = pickle.load(fp)
    else:
        classifier = None
        kernels = None

    run_hyperparam_grid(pg, data, cpts, data.index[0], data.index[-1], kernels, classifier, margin=500)
